package org.bitbucket.anuarkaliyev23.example.blog.configuration;

import com.j256.ormlite.support.ConnectionSource;

public interface DatabaseConfiguration {
    String jdbcConnectionString();
    ConnectionSource connectionSource();
}
