package org.bitbucket.anuarkaliyev23.example.blog.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.ForbiddenResponse;
import io.javalin.http.InternalServerErrorResponse;
import org.bitbucket.anuarkaliyev23.example.blog.configuration.HttpConstants;
import org.bitbucket.anuarkaliyev23.example.blog.configuration.ObjectMapperFactory;
import org.bitbucket.anuarkaliyev23.example.blog.model.application.ModelPermission;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.Post;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.User;
import org.bitbucket.anuarkaliyev23.example.blog.service.Service;
import org.bitbucket.anuarkaliyev23.example.blog.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

public class PostController implements AuthorizationController<Post> {
    private final Service<Post> postService;
    private final UserService userService;
    private final ObjectMapperFactory objectMapperFactory;

    public PostController(Service<Post> postService, UserService userService, ObjectMapperFactory objectMapperFactory) {
        this.postService = postService;
        this.userService = userService;
        this.objectMapperFactory = objectMapperFactory;
    }

    public Service<Post> getPostService() {
        return postService;
    }

    public ObjectMapperFactory getObjectMapperFactory() {
        return objectMapperFactory;
    }

    public UserService getUserService() {
        return userService;
    }

    @Override
    public void create(Context context) {
        try {
            User sender = senderOrThrowUnauthorized(context);
            Post payload = objectMapperFactory.objectMapper(ModelPermission.CREATE).readValue(context.body(), Post.class);

            if (userService.permissionsFor(sender, payload).contains(ModelPermission.CREATE)) {
                postService.save(payload);
                context.status(HttpConstants.HTTP_201_CREATED);
            }
        } catch (JsonProcessingException jpe) {
            jpe.printStackTrace();
            throw new BadRequestResponse();
        }
    }

    @Override
    public void getAll(Context context) {
        try {
            User sender = senderOrThrowUnauthorized(context);
            List<Post> allowedPost = postService.all()
                    .stream()
                    .filter(post -> userService.permissionsFor(sender, post).contains(ModelPermission.READ))
                    .collect(Collectors.toList());
            context.result(objectMapperFactory.objectMapper(ModelPermission.READ).writeValueAsString(allowedPost));
        } catch (JsonProcessingException jpe) {
            jpe.printStackTrace();
            throw new InternalServerErrorResponse();
        }
    }

    @Override
    public void getOne(Context context, int id) {
        try {
            User sender = senderOrThrowUnauthorized(context);
            Post targetPost = postService.findById(id);
            if (userService.permissionsFor(sender, targetPost).contains(ModelPermission.READ)) {
                context.result(objectMapperFactory.objectMapper(ModelPermission.READ).writeValueAsString(targetPost));

            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new InternalServerErrorResponse();
        }
    }

    @Override
    public void update(Context context, int id) {
        try {
            User sender = senderOrThrowUnauthorized(context);
            Post targetPost = postService.findById(id);
            if (userService.permissionsFor(sender, targetPost).contains(ModelPermission.UPDATE)) {
                Post updated = objectMapperFactory.objectMapper(ModelPermission.UPDATE).readValue(context.body(), Post.class);
                updated.setId(id);
                postService.update(updated);
                context.result(objectMapperFactory.objectMapper(ModelPermission.UPDATE).writeValueAsString(updated));
            } else {
                throw new ForbiddenResponse();
            }
        } catch (JsonProcessingException jpe) {
            jpe.printStackTrace();
            throw new BadRequestResponse();
        }
    }

    @Override
    public void delete(Context context, int id) {
        User sender = senderOrThrowUnauthorized(context);
        Post target = postService.findById(id);
        if (userService.permissionsFor(sender, target).contains(ModelPermission.DELETE)) {
            postService.deleteById(id);
            context.status(HttpConstants.HTTP_204_NO_CONTENT);
        } else {
            throw new ForbiddenResponse();
        }

    }


    @Override
    public UserService userService() {
        return userService;
    }
}
