package org.bitbucket.anuarkaliyev23.example.blog.json.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.User;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.UserStatus;
import org.mindrot.jbcrypt.BCrypt;

import java.io.IOException;
import java.time.LocalDate;

public class UserDeserializer extends StdDeserializer<User> {
    public UserDeserializer() {
        super(User.class);
    }

    @Override
    public User deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode root = jsonParser.getCodec().readTree(jsonParser);
        String login = root.get(User.COLUMN_LOGIN_NAME).asText();
        String password = root.get(User.COLUMN_PASSWORD_NAME).asText();
        UserStatus status = UserStatus.valueOf(root.get(User.COLUMN_STATUS_NAME).asText().toUpperCase());
        String hashed = BCrypt.hashpw(password, BCrypt.gensalt());
        return new User(0, login, hashed, LocalDate.now(), status);
    }
}
