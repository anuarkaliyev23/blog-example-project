package org.bitbucket.anuarkaliyev23.example.blog.service;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import org.bitbucket.anuarkaliyev23.example.blog.configuration.DatabaseConfiguration;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.VipApplication;

import java.sql.SQLException;

public class SimpleVipApplicationService implements Service<VipApplication> {
    private final DatabaseConfiguration databaseConfiguration;

    public SimpleVipApplicationService(DatabaseConfiguration databaseConfiguration) {
        this.databaseConfiguration = databaseConfiguration;
    }

    public DatabaseConfiguration getDatabaseConfiguration() {
        return databaseConfiguration;
    }

    @Override
    public Dao<VipApplication, Integer> dao() {
        try {
            return DaoManager.createDao(databaseConfiguration.connectionSource(), VipApplication.class);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new RuntimeException("Unable to create dao for VipApplication model");
        }
    }
}
