package org.bitbucket.anuarkaliyev23.example.blog.controller;

import io.javalin.http.Context;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.Model;

public interface Controller<T extends Model> {
    void create(Context context);
    void getAll(Context context);
    void getOne(Context context, int id);
    void update(Context context, int id);
    void delete(Context context, int id);
}