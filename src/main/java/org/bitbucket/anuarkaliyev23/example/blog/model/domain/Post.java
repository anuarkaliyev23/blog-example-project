package org.bitbucket.anuarkaliyev23.example.blog.model.domain;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.time.LocalDate;
import java.util.Objects;

@DatabaseTable
public class Post implements Model {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private String heading;
    @DatabaseField
    private String text;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private LocalDate createdAt;
    @DatabaseField
    private boolean vipAccessible;

    public Post() {
    }

    public Post(int id, String heading, String text, LocalDate createdAt, boolean vipAccessible) {
        this.id = id;
        this.heading = heading;
        this.text = text;
        this.createdAt = createdAt;
        this.vipAccessible = vipAccessible;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isVipAccessible() {
        return vipAccessible;
    }

    public void setVipAccessible(boolean vipAccessible) {
        this.vipAccessible = vipAccessible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return id == post.id &&
                vipAccessible == post.vipAccessible &&
                Objects.equals(heading, post.heading) &&
                Objects.equals(text, post.text) &&
                Objects.equals(createdAt, post.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, heading, text, createdAt, vipAccessible);
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", heading='" + heading + '\'' +
                ", text='" + text + '\'' +
                ", createdAt=" + createdAt +
                ", vipAccessible=" + vipAccessible +
                '}';
    }

    public static final String COLUMN_ID_NAME = "id";
    public static final String COLUMN_HEADING_NAME = "heading";
    public static final String COLUMN_TEXT_NAME = "text";
    public static final String COLUMN_CREATED_AT_NAME  = "createdAt";
    public static final String COLUMN_VIP_ACCESSIBLE_NAME = "vipAccessible";

}
