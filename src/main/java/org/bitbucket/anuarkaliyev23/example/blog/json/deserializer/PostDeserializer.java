package org.bitbucket.anuarkaliyev23.example.blog.json.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.Post;

import java.io.IOException;
import java.time.LocalDate;

public class PostDeserializer extends StdDeserializer<Post> {
    public PostDeserializer() {
        super(Post.class);
    }

    @Override
    public Post deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode root = jsonParser.getCodec().readTree(jsonParser);
        String heading = root.get(Post.COLUMN_HEADING_NAME).asText();
        String text = root.get(Post.COLUMN_TEXT_NAME).asText();
        boolean vipAccessible = root.get(Post.COLUMN_VIP_ACCESSIBLE_NAME).asBoolean();
        return new Post(0, heading, text, LocalDate.now(), vipAccessible);
    }
}
