package org.bitbucket.anuarkaliyev23.example.blog;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.module.SimpleModule;
import io.javalin.Javalin;
import org.bitbucket.anuarkaliyev23.example.blog.configuration.DatabaseConfiguration;
import org.bitbucket.anuarkaliyev23.example.blog.configuration.ModelPermissionObjectMapperFactory;
import org.bitbucket.anuarkaliyev23.example.blog.configuration.ObjectMapperFactory;
import org.bitbucket.anuarkaliyev23.example.blog.configuration.SimpleDatabaseConfiguration;
import org.bitbucket.anuarkaliyev23.example.blog.controller.*;
import org.bitbucket.anuarkaliyev23.example.blog.json.deserializer.PostDeserializer;
import org.bitbucket.anuarkaliyev23.example.blog.json.deserializer.UserDeserializer;
import org.bitbucket.anuarkaliyev23.example.blog.json.serializer.UserSerializer;
import org.bitbucket.anuarkaliyev23.example.blog.model.application.ModelPermission;
import org.bitbucket.anuarkaliyev23.example.blog.model.application.Permission;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.Post;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.User;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.VipApplication;
import org.bitbucket.anuarkaliyev23.example.blog.service.*;

import java.util.HashMap;
import java.util.Map;

import static io.javalin.apibuilder.ApiBuilder.*;

public class Main {
    public static void main(String[] args) {
        Javalin app = Javalin.create(javalinConfig -> {
            javalinConfig.prefer405over404 = true;
            javalinConfig.showJavalinBanner = false;
            javalinConfig.enableDevLogging();
            javalinConfig.enableCorsForAllOrigins();
            javalinConfig.defaultContentType = "application/json";
        });

        SimpleModule module = new SimpleModule();
        module.addSerializer(new UserSerializer())
                .addDeserializer(User.class, new UserDeserializer())
                .addDeserializer(Post.class, new PostDeserializer());

        Map<Permission, Module> permissionModuleMap = new HashMap<>();
        permissionModuleMap.put(ModelPermission.CREATE, module);
        permissionModuleMap.put(ModelPermission.READ, module);
        permissionModuleMap.put(ModelPermission.UPDATE, module);
        ObjectMapperFactory mapperFactory = new ModelPermissionObjectMapperFactory(permissionModuleMap);

        DatabaseConfiguration configuration = new SimpleDatabaseConfiguration();

        Service<Post> postService = new SimplePostService(configuration);
        Service<VipApplication> vipApplicationService = new SimpleVipApplicationService(configuration);
        UserService userService = new SimpleUserService(configuration);

        Controller<Post> postController = new PostController(postService, userService, mapperFactory);
        Controller<VipApplication> vipApplicationController = new VipApplicationController(vipApplicationService, userService, mapperFactory);
        Controller<User> userController = new UserController(userService, mapperFactory);

        app.routes(() -> {
            path("users", () -> {
                get(userController::getAll);
                post(userController::create);
                path(PATH_PARAM_ID, () -> {
                    get(ctx -> userController.getOne(ctx,ctx.pathParam(PATH_PARAM_ID, Integer.class).get()));
                    patch(ctx -> userController.update(ctx, ctx.pathParam(PATH_PARAM_ID, Integer.class).get()));
                    delete(ctx -> userController.delete(ctx, ctx.pathParam(PATH_PARAM_ID, Integer.class).get()));
                });
            });
            path("posts", () -> {
                get(postController::getAll);
                post(postController::create);
                path(PATH_PARAM_ID, () -> {
                    get(ctx -> postController.getOne(ctx, ctx.pathParam(PATH_PARAM_ID, Integer.class).get()));
                    patch(ctx -> postController.update(ctx, ctx.pathParam(PATH_PARAM_ID, Integer.class).get()));
                    delete(ctx -> postController.delete(ctx, ctx.pathParam(PATH_PARAM_ID, Integer.class).get()));
                });
            });
            path("applications", () -> {
                get(vipApplicationController::getAll);
                post(vipApplicationController::create);
                path(PATH_PARAM_ID, () -> {
                    delete(ctx -> vipApplicationController.delete(ctx, ctx.pathParam(PATH_PARAM_ID, Integer.class).get()));
                });
            });
        });

        app.start(6543);
    }

    public static final String PATH_PARAM_ID = ":id";
}
