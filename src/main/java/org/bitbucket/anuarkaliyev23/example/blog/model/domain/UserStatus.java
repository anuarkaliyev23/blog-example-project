package org.bitbucket.anuarkaliyev23.example.blog.model.domain;

public enum UserStatus {
    GENERIC,
    VIP,
    ADMIN
}
