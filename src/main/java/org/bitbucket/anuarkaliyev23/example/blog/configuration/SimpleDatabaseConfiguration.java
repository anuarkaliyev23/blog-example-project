package org.bitbucket.anuarkaliyev23.example.blog.configuration;


import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.Post;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.User;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.VipApplication;

import java.sql.SQLException;

public class SimpleDatabaseConfiguration implements DatabaseConfiguration {
    static final String JDBC_CONNECTION_STRING = "jdbc:sqlite:C:\\Users\\Khambar\\Desktop\\MnC\\backend\\blog.db";
    private static ConnectionSource connectionSource = null;

    static {
        try {
            connectionSource = new JdbcPooledConnectionSource(JDBC_CONNECTION_STRING);

            TableUtils.createTableIfNotExists(connectionSource, User.class);
            TableUtils.createTableIfNotExists(connectionSource, Post.class);
            TableUtils.createTableIfNotExists(connectionSource, VipApplication.class);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new RuntimeException("Unable to initialize database configuration");
        }
    }

    @Override
    public String jdbcConnectionString() {
        return JDBC_CONNECTION_STRING;
    }

    @Override
    public ConnectionSource connectionSource() {
        if (connectionSource == null)
            throw new RuntimeException("Have not initialized ConnectionSource properly");
        return connectionSource;
    }
}
