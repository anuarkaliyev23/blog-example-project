package org.bitbucket.anuarkaliyev23.example.blog.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.anuarkaliyev23.example.blog.model.application.Permission;

public interface ObjectMapperFactory {
    ObjectMapper objectMapper(Permission permission);
}
