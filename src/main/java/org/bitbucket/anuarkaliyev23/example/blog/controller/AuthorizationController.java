package org.bitbucket.anuarkaliyev23.example.blog.controller;

import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.Model;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.User;
import org.bitbucket.anuarkaliyev23.example.blog.service.UserService;

public interface AuthorizationController<T extends Model> extends Controller<T> {

    UserService userService();

    default User sender(Context context) {
        if (!context.basicAuthCredentialsExist())
            return null;

        String username = context.basicAuthCredentials().getUsername();
        String passwordPlain = context.basicAuthCredentials().getPassword();
        return userService().authenticate(username, passwordPlain);
    }

    default User senderOrThrowUnauthorized(Context context) {
        User user = sender(context);
        if (user == null)
            throw new UnauthorizedResponse();
        return user;
    }
}
