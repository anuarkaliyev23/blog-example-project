package org.bitbucket.anuarkaliyev23.example.blog.model.domain;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.time.LocalDate;
import java.util.Objects;

@DatabaseTable
public class VipApplication implements Model {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private User user;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private LocalDate createdAt;

    public VipApplication() {
    }

    public VipApplication(int id, User user, LocalDate createdAt) {
        this.id = id;
        this.user = user;
        this.createdAt = createdAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VipApplication that = (VipApplication) o;
        return id == that.id &&
                Objects.equals(user, that.user) &&
                Objects.equals(createdAt, that.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, createdAt);
    }

    @Override
    public String toString() {
        return "VipApplication{" +
                "id=" + id +
                ", user=" + user +
                ", date=" + createdAt +
                '}';
    }

    public static final String COLUMN_ID_NAME = "id";
    public static final String COLUMN_USER_NAME = "user";
    public static final String COLUMN_DATE_NAME = "createdAt";
}
