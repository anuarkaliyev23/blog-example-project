package org.bitbucket.anuarkaliyev23.example.blog.configuration;

public interface HttpConstants {
    int HTTP_200_OK = 200;
    int HTTP_201_CREATED = 201;
    int HTTP_204_NO_CONTENT = 204;
}