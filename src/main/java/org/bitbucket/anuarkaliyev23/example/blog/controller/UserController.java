package org.bitbucket.anuarkaliyev23.example.blog.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.ForbiddenResponse;
import io.javalin.http.InternalServerErrorResponse;
import org.bitbucket.anuarkaliyev23.example.blog.configuration.HttpConstants;
import org.bitbucket.anuarkaliyev23.example.blog.configuration.ObjectMapperFactory;
import org.bitbucket.anuarkaliyev23.example.blog.model.application.ModelPermission;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.User;
import org.bitbucket.anuarkaliyev23.example.blog.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

public class UserController implements AuthorizationController<User> {
    private final UserService userService;
    private final ObjectMapperFactory objectMapperFactory;

    public UserController(UserService userService, ObjectMapperFactory objectMapperFactory) {
        this.userService = userService;
        this.objectMapperFactory = objectMapperFactory;
    }

    public UserService getUserService() {
        return userService;
    }

    public ObjectMapperFactory getObjectMapperFactory() {
        return objectMapperFactory;
    }

    @Override
    public void create(Context context) {
        try {
            User sender = sender(context);
            User payload = objectMapperFactory.objectMapper(ModelPermission.CREATE).readValue(context.body(), User.class);

            if (userService.permissionsFor(sender, payload).contains(ModelPermission.CREATE)) {
                userService.save(payload);
                context.status(HttpConstants.HTTP_201_CREATED);
            } else {
                throw new ForbiddenResponse();
            }
        } catch (JsonProcessingException jpe) {
            jpe.printStackTrace();
            throw new BadRequestResponse();
        }
    }

    @Override
    public void getAll(Context context) {
        try {
            User sender = senderOrThrowUnauthorized(context);

            List<User> result = userService.all()
                    .stream()
                    .filter(user -> userService.permissionsFor(sender, user).contains(ModelPermission.READ))
                    .collect(Collectors.toList());
            context.result(objectMapperFactory.objectMapper(ModelPermission.CREATE).writeValueAsString(result));
        } catch (JsonProcessingException jpe) {
            jpe.printStackTrace();
            throw new InternalServerErrorResponse();
        }
    }

    @Override
    public void getOne(Context context, int id) {
        try {
            User sender = senderOrThrowUnauthorized(context);
            User target = userService.findById(id);
            if (userService.permissionsFor(sender, target).contains(ModelPermission.READ)) {
                context.result(objectMapperFactory.objectMapper(ModelPermission.READ).writeValueAsString(target));
            } else {
                throw new ForbiddenResponse();
            }
        } catch (JsonProcessingException jpe) {
            jpe.printStackTrace();
            throw new InternalServerErrorResponse();
        }
    }

    @Override
    public void update(Context context, int id) {
        try {
            User sender = senderOrThrowUnauthorized(context);
            User target = userService.findById(id);

            if (userService.permissionsFor(sender, target).contains(ModelPermission.UPDATE)) {
                context.result(objectMapperFactory.objectMapper(ModelPermission.UPDATE).writeValueAsString(target));
                User updated = objectMapperFactory.objectMapper(ModelPermission.UPDATE).readValue(context.body(), User.class);
                updated.setId(id);
                userService.update(updated);
                context.result(objectMapperFactory.objectMapper(ModelPermission.UPDATE).writeValueAsString(updated));
            } else {
                throw new ForbiddenResponse();
            }
        } catch (JsonProcessingException jpe) {
            jpe.printStackTrace();
            throw new BadRequestResponse();
        }
    }

    @Override
    public void delete(Context context, int id) {

        User sender = senderOrThrowUnauthorized(context);
        User target = userService.findById(id);
        if (userService.permissionsFor(sender, target).contains(ModelPermission.DELETE)) {
            userService.deleteById(id);
            context.status(HttpConstants.HTTP_204_NO_CONTENT);
        } else {
            throw new ForbiddenResponse();
        }

    }

    @Override
    public UserService userService() {
        return userService;
    }
}
