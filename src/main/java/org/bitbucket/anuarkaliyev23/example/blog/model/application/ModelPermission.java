package org.bitbucket.anuarkaliyev23.example.blog.model.application;

public enum ModelPermission implements Permission {
    CREATE,
    READ,
    UPDATE,
    DELETE;
}
