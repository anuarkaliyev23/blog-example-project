package org.bitbucket.anuarkaliyev23.example.blog.json.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.User;

import java.io.IOException;

public class UserSerializer extends StdSerializer<User> {
    public UserSerializer() {
        super(User.class);
    }

    @Override
    public void serialize(User user, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField(User.COLUMN_ID_NAME, user.getId());
        jsonGenerator.writeStringField(User.COLUMN_LOGIN_NAME, user.getLogin());
        jsonGenerator.writeStringField(User.COLUMN_STATUS_NAME, user.getStatus().name());
        jsonGenerator.writeObjectField(User.COLUMN_CREATED_AT_NAME, user.getCreatedAt());
        jsonGenerator.writeEndObject();
    }
}
