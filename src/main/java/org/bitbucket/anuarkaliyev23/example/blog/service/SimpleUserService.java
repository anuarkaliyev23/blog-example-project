package org.bitbucket.anuarkaliyev23.example.blog.service;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import org.bitbucket.anuarkaliyev23.example.blog.configuration.DatabaseConfiguration;
import org.bitbucket.anuarkaliyev23.example.blog.model.application.ModelPermission;
import org.bitbucket.anuarkaliyev23.example.blog.model.application.Permission;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.Post;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.User;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.UserStatus;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.VipApplication;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class SimpleUserService implements UserService {
    private final DatabaseConfiguration databaseConfiguration;

    public SimpleUserService(DatabaseConfiguration databaseConfiguration) {
        this.databaseConfiguration = databaseConfiguration;
    }

    public DatabaseConfiguration getDatabaseConfiguration() {
        return databaseConfiguration;
    }

    @Override
    public Dao<User, Integer> dao() {
        try {
            return DaoManager.createDao(databaseConfiguration.connectionSource(), User.class);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new RuntimeException("Unable to create dao for User model");
        }
    }

    @Override
    public User authenticate(String login, String password) {
        if (!loginExists(login))
            throw new RuntimeException(String.format("Cannot authenticate user with login %s. It doesn't exist", login));

        User user = findByLogin(login);
        if (BCrypt.checkpw(password, user.getPassword()))
                return user;
        else return null;
    }

    @Override
    public boolean loginExists(String login) {
        return findByLogin(login) != null;
    }

    @Override
    public User findByLogin(String login) {
        try {
            QueryBuilder<User, Integer> queryBuilder = dao().queryBuilder();
            queryBuilder.where().eq(User.COLUMN_LOGIN_NAME, login);
            PreparedQuery<User> preparedQuery = queryBuilder.prepare();
            User user = dao().queryForFirst(preparedQuery);
            return user;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new RuntimeException("SQL exception occurred");
        }
    }

    @Override
    public Set<Permission> permissionsFor(User user, Post post) {
        Set<Permission> permissions = new HashSet<>();
        if (user.getStatus() == UserStatus.ADMIN) {
            permissions.addAll(Arrays.asList(ModelPermission.values()));
        } else if (user.getStatus() == UserStatus.VIP) {
            permissions.add(ModelPermission.READ);
        } else if (user.getStatus() == UserStatus.GENERIC) {
            if (!post.isVipAccessible()) {
                permissions.add(ModelPermission.READ);
            }
        }
        return permissions;
    }

    @Override
    public Set<Permission> permissionsFor(User user, User target) {
        Set<Permission> permissions = new HashSet<>();
        if (user == null) {
            permissions.add(ModelPermission.CREATE);
            return permissions;
        }
        if (user.getStatus() == UserStatus.ADMIN) {
            if (target.getStatus() != UserStatus.ADMIN) {
                permissions.add(ModelPermission.DELETE);
            }
            permissions.addAll(Arrays.stream(ModelPermission.values()).filter(permission -> permission != ModelPermission.DELETE).collect(Collectors.toSet()));
        } else {
            permissions.add(ModelPermission.READ);
        }
        return permissions;
    }

    @Override
    public Set<Permission> permissionsFor(User user, VipApplication vipApplication) {
        Set<Permission> permissions = new HashSet<>();
        if (user.getStatus() == UserStatus.GENERIC) {
            permissions.add(ModelPermission.CREATE);
        } else if (user.getStatus() == UserStatus.ADMIN) {
            permissions.add(ModelPermission.READ);
        }
        return permissions;
    }
}
