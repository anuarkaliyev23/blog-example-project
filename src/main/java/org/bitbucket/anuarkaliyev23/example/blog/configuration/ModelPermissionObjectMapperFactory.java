package org.bitbucket.anuarkaliyev23.example.blog.configuration;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.bitbucket.anuarkaliyev23.example.blog.model.application.Permission;

import java.util.Map;

public class ModelPermissionObjectMapperFactory implements ObjectMapperFactory{
    private Map<Permission, Module> permissions;

    public ModelPermissionObjectMapperFactory() {
    }

    public ModelPermissionObjectMapperFactory(Map<Permission, Module> permissions) {
        this.permissions = permissions;
    }

    public Map<Permission, Module> getPermissions() {
        return permissions;
    }

    public void setPermissions(Map<Permission, Module> permissions) {
        this.permissions = permissions;
    }

    @Override
    public ObjectMapper objectMapper(Permission permission) {
        ObjectMapper mapper = new ObjectMapper();
        Module module = permissions.get(permission);
        mapper.registerModule(module)
                .registerModule(new JavaTimeModule());
        return mapper;
    }
}
