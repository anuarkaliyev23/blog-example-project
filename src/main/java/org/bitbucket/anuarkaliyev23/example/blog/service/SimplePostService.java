package org.bitbucket.anuarkaliyev23.example.blog.service;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import org.bitbucket.anuarkaliyev23.example.blog.configuration.DatabaseConfiguration;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.Post;

import java.sql.SQLException;

public class SimplePostService implements Service<Post> {
    private final DatabaseConfiguration databaseConfiguration;

    public SimplePostService(DatabaseConfiguration databaseConfiguration) {
        this.databaseConfiguration = databaseConfiguration;
    }

    public DatabaseConfiguration getDatabaseConfiguration() {
        return databaseConfiguration;
    }

    @Override
    public Dao<Post, Integer> dao() {
        try {
            return DaoManager.createDao(databaseConfiguration.connectionSource(), Post.class);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new RuntimeException("Unable to create dao for Post model");
        }
    }
}
