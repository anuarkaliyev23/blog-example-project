package org.bitbucket.anuarkaliyev23.example.blog.service;

import org.bitbucket.anuarkaliyev23.example.blog.model.application.Permission;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.Post;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.User;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.VipApplication;

import java.util.Set;

public interface UserService extends Service<User> {
    User authenticate(String login, String password);
    boolean loginExists(String login);
    User findByLogin(String login);
    Set<Permission> permissionsFor(User user, Post post);
    Set<Permission> permissionsFor(User user, User target);
    Set<Permission> permissionsFor(User user, VipApplication vipApplication);
}
