package org.bitbucket.anuarkaliyev23.example.blog.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.javalin.http.*;
import org.bitbucket.anuarkaliyev23.example.blog.configuration.HttpConstants;
import org.bitbucket.anuarkaliyev23.example.blog.configuration.ObjectMapperFactory;
import org.bitbucket.anuarkaliyev23.example.blog.model.application.ModelPermission;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.User;
import org.bitbucket.anuarkaliyev23.example.blog.model.domain.VipApplication;
import org.bitbucket.anuarkaliyev23.example.blog.service.Service;
import org.bitbucket.anuarkaliyev23.example.blog.service.UserService;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class VipApplicationController implements AuthorizationController<VipApplication>  {
    private final Service<VipApplication> vipApplicationService;
    private final UserService userService;
    private final ObjectMapperFactory objectMapperFactory;

    public VipApplicationController(Service<VipApplication> vipApplicationService, UserService userService, ObjectMapperFactory objectMapperFactory) {
        this.vipApplicationService = vipApplicationService;
        this.userService = userService;
        this.objectMapperFactory = objectMapperFactory;
    }

    @Override
    public UserService userService() {
        return userService;
    }

    @Override
    public void create(Context context) {
        User sender = senderOrThrowUnauthorized(context);
        VipApplication application = new VipApplication(0, sender, LocalDate.now());

        if (userService.permissionsFor(sender, application).contains(ModelPermission.CREATE)) {
            vipApplicationService.save(application);
            context.status(HttpConstants.HTTP_201_CREATED);
        } else {
            throw new ForbiddenResponse();
        }
    }

    @Override
    public void getAll(Context context) {
        try {
            User sender = senderOrThrowUnauthorized(context);
            List<VipApplication> result = vipApplicationService.all()
                    .stream()
                    .filter(vipApplication -> userService.permissionsFor(sender, vipApplication).contains(ModelPermission.READ))
                    .collect(Collectors.toList());
            context.result(objectMapperFactory.objectMapper(ModelPermission.READ).writeValueAsString(result));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new InternalServerErrorResponse();
        }
    }

    @Override
    public void getOne(Context context, int id) {
        try {
            User sender = senderOrThrowUnauthorized(context);
            VipApplication application = vipApplicationService.findById(id);

            if (userService.permissionsFor(sender, application).contains(ModelPermission.READ)) {
                context.result(objectMapperFactory.objectMapper(ModelPermission.READ).writeValueAsString(vipApplicationService));
            } else {
                throw new ForbiddenResponse();
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new InternalServerErrorResponse();
        }
    }

    @Override
    public void update(Context context, int id) {
        throw new MethodNotAllowedResponse("Update not allowed", Collections.emptyMap());
    }

    @Override
    public void delete(Context context, int id) {
        throw new MethodNotAllowedResponse("Delete not allowed", Collections.emptyMap());
    }
}
