package org.bitbucket.anuarkaliyev23.example.blog.model.domain;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.time.LocalDate;
import java.util.Objects;

@DatabaseTable
public class User implements Model {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(unique = true)
    private String login;
    @DatabaseField
    private String password;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private LocalDate createdAt;
    @DatabaseField(dataType = DataType.ENUM_STRING)
    private UserStatus status;

    public User() {
    }

    public User(int id, String login, String password, LocalDate createdAt, UserStatus status) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.createdAt = createdAt;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                Objects.equals(createdAt, user.createdAt) &&
                status == user.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, password, createdAt, status);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", createdAt=" + createdAt +
                ", status=" + status +
                '}';
    }
    public static final String COLUMN_ID_NAME = "id";
    public static final String COLUMN_LOGIN_NAME = "login";
    public static final String COLUMN_PASSWORD_NAME = "password";
    public static final String COLUMN_CREATED_AT_NAME = "createdAt";
    public static final String COLUMN_STATUS_NAME = "status";


}
